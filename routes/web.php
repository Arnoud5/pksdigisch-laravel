<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/// crud table game
// create
Route::get('/game/create','GameController@create'); //mengarah ke form tambah cast 
Route::post('/game','GameController@store'); //menyimpan data form ke database table cast


// read
Route::get('/game','GameController@index'); //menampilkan semua data dari database table cast ke blade
Route::get('/game/{game_id}','GameController@show'); //menampilkan detail data per id


// update
Route::get('/game/{game_id}/edit','GameController@edit'); //mengarah ke form mengedit data tabel cast by id

Route::put('/game/{game_id}','GameController@update'); //buat update data di database


// delete
Route::delete('/game/{game_id}','GameController@destroy'); //route delete data berdsarkan id