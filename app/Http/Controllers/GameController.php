<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $game = Game::all();
        return view('game.Index',compact('game'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('game.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:45',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ],
        [
            'name.required' => 'nama harus diisi',
            'gameplay.required' => 'Umur harus diisi',
            'developer.required' => 'bio harus diisi',
            'year.required' => 'bio harus diisi',
        ]
    );

    $game = new Game;
    // ambil dari
    // database    bladde
    $game->name = $request->name;
    $game->gameplay = $request->gameplay;
    $game->developer = $request->developer;
    $game->year = $request->year;
    
    $game->save();
    return redirect('/game');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = Game::find($id)->first();
        return view('game.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = Game::find($id)->first();
        return view('game.show', compact('game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:45',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ],
        [
            'name.required' => 'nama harus diisi',
            'gameplay.required' => 'Umur harus diisi',
            'developer.required' => 'bio harus diisi',
            'year.required' => 'bio harus diisi',
        ]
    );

    $game = Game::find($id);

    $game->name = $request['name'];
    $game->gameplay = $request['gameplay'];
    $game->developer = $request['developer'];
    $game->year = $request['year'];
    
    $game->save();

    return redirect('/game');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $game = Game::find($id);
 
        $game->delete();

        return redirect('/game');
    }
}
